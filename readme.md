# Isometric Game
Just a little fun isometric-based game I am working on. Going to see what I can accomplish.

## How to run on Windows (gitbash):
	git clone "https://gitlab.com/Anti_Alias/IsometricGame.git"
	cd IsometricGame
	./gradlew.bat desktop:run

## How to run on Linux/Mac/Unix/Posix systems:
	git clone "https://gitlab.com/Anti_Alias/IsometricGame.git"
	cd IsometricGame
	./gradlew desktop:run