package game.isometric

import engine.logic.Game
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture


/**
 * Represents the application root that allocates initial resources and is
 * in control of main game loop.
 */
class Main : ApplicationAdapter()
{
	lateinit var img: Texture

	/**
	 * Allocates initial resources.
	 * Invoked after OpenGL and OpenAL are initialized.
	 */
	override fun create()
	{
		Game.create(Gdx.graphics.width, Gdx.graphics.height)
	}

	/**
	 * Main game loop / rendering loop.
	 * Invoked on vsync
	 */
	override fun render()
	{
		Game.run()
		Game.render()
	}

	/**
	 * Disposes of all resources.
	 * Invoked when application is closed.
	 */
	override fun dispose()
	{
		Game.dispose()
	}
}
