package engine.logic

import kotlin.reflect.KClass

/**
 * Represents a Component of an Entity.
 * Merely acts as a bag of data to be manipulated
 * by a System or Systems.
 */
interface Component
{
    /**
     * Disposes of Component if applicable.
     * Default implementation does nothing.
     */
    fun dispose():Unit{}

    /**
     * Helpful Companion object
     */
    companion object
    {
        private var index:Int = 0
        private var _classes:List<KClass<out Component>> = listOf()

        /**
         * Returns index of class.
         * This method is synchronized.
         */
        fun <T : Component> typeIndexOf(c: KClass<T>):Int = synchronized(this)
        {
            val index:Int = _classes.indexOf(c)
            return if(index == -1)
            {
                _classes += c
                _classes.size-1
            }
            else index
        }

        /**
         * Number of Component types loaded.
         */
        val numLoaded:Int get() = _classes.size
    }
}