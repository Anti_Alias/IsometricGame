package engine.logic
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch

/**
 * Represents the main Game object singleton.
 * Has global information about application and
 * contains a 'World' where entities and terrain live.
 */
object Game
{
    // Private vars
    private var _hasCreated = false
    private var _width = -1
    private var _height = -1
    private var _world:World? = null
    lateinit var _batch: SpriteBatch



    /**
     * Horizontal resolution of Game.
     */
    val width:Int get()
    {
        checkCreate()
        return _width
    }

    /**
     * Vertical resolution of Game.
     */
    val height:Int get()
    {
        checkCreate()
        return _height
    }

    // Initialization code
    fun create(width:Int, height:Int)
    {
        checkCreate()
        require(width > 0 && height > 0){"Invalid dimensions $width, $height"}
        _width = width
        _height = height
        _batch = SpriteBatch()
    }

    /**
     * Possible World this Game is running.
     */
    val world:World? get() = _world

    /**
     * Runs logical portion of a game tick.
     */
    fun run()
    {
        world?.run()
    }

    /**
     * Runs graphical portion of a game tick.
     * Should be invoked after run()
     */
    fun render()
    {
        // Clears screen
        Gdx.gl.glClearColor(1f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // Renders World's graphics
        world?.render()
    }

    /**
     * Disposes of resources.
     */
    fun dispose()
    {
        println("Disposing...")
        _batch.dispose()
    }

    /**
     * Checks initialization status
     */
    private fun checkCreate()
    {
        assert(!_hasCreated){"Game cannot be initialized twice."}
    }
}