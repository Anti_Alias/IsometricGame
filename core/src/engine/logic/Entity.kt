package engine.logic
import kotlin.reflect.KClass


/**
 * Represents a game object who holds data in its Components.
 * @param name Name of the Entity
 * @param component variable number of Components to start with.
 */
class Entity(val name:String, vararg component:Component)
{
    private var _components: Array<Any?> = arrayOfNulls(Component.numLoaded+2)
    private var _indices: IntArray = IntArray(_components.size)
    private var _indicesSize:Int = 0

    /**
     * Clears index list
     */
    init
    {
       for(i in 0 until _indices.size)
           _indices[i] = -1
    }

    /**
     * Adds a Component to the Entity.
     * Throws exception if Component of same already stored.
     */
    fun addComponent(c: Component):Unit
    {
        assert(!containsType(c::class)){"Component of class stored twice."}
        val index:Int = Component.typeIndexOf(c::class)
        if(index >= _components.size)
        {
            _components = _components.copyOf(index * 3 / 2)
            _indices = _indices.copyOf(_components.size)
        }
        _components[index] = c
        _indices[_indicesSize++] = index
    }

    fun removeComponent(c: Component):Unit
    {
        val index:Int = Component.typeIndexOf(c::class)
        assert(index <= _indicesSize){"Component not stored."}
        val elem: Any? = _components[index]
        assert(elem == c){"Component not stored"}
        _components[index] = null
        for(i in 0 until _indices.size)
        {
            if(_indices[i] == index)
                _indices[i] = -1
        }
    }

    /**
     * Checks if Component is in this Entity
     */
    operator fun contains(c: Component):Boolean = indexOf(c) != -1

    /**
     * Gets index of Component
     */
    fun <T : Component> indexOfType(clazz: KClass<T>):Int
    {
        for(i in 0 until _indices.size)
        {
            val index = _indices[i]
            val comp:Any = _components[index]!!
            if(comp::class == clazz)
                return index
        }
        return -1
    }


    /**
     * Gets index of Component
     */
    fun <T : Component> indexOf(c: T):Int
    {
        for(i in 0 until _indices.size)
        {
            val index = _indices[i]
            val comp:Any? = _components[index]
            if(c == comp)
                return index
        }
        return -1
    }

    /**
     * @return true if this Entity contains a Component of the specified KClass.
     */
    fun <T : Component> containsType(clazz: KClass<T>):Boolean = indexOfType(clazz) != -1

    /**
     * Disposes of all Components
     */
    fun dispose():Unit
    {

    }
}