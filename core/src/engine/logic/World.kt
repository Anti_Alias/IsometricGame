package engine.logic

/**
 * Represents a World in a Game that is occupied by Entities
 */
class World
{
    private var _entityAddListeners:List<(Entity)->Unit> = listOf()
    private var _entityRemoveListeners:List<(Entity)->Unit> = listOf()
    private var _entities:List<Entity> = listOf()

    /**
     * Read-only list of Entities.
     */
    val entities:List<Entity> get() = _entities

    /**
     * Adds Entity, and invokes all add listeners
     */
    fun addEntity(e:Entity):Unit
    {
        if(e !in _entities)
        {
            _entities += e
            _entityAddListeners.forEach { it.invoke(e) }
        }
    }

    /**
     * Removes Entity and invokes all remove listeners
     */
    fun removeEntity(e:Entity):Unit
    {
        val size = _entities.size
        _entities -= e
        if(size != _entities.size)
            _entityRemoveListeners.forEach{it.invoke(e)}
    }

    /**
     * Adds a listener that will be invoked whenever an Entity is added to the World
     */
    fun addEntityAddListener(listener: (Entity)->Unit)
    {
        _entityAddListeners += listener
    }

    /**
     * Adds a listener that will be invoked whenever an Entity is removed from the World
     */
    fun addEntityRemoveListener(listener: (Entity)->Unit)
    {
        _entityRemoveListeners += listener
    }


    /**
     * Runs the World
     */
    fun run()
    {

    }

    /**
     * Renders World's graphics
     */
    fun render()
    {

    }


    /**
     * Disposes resources this World uses. This includes terrain graphics
     * and entities.
     */
    fun dispose():Unit
    {
        // Disposes of disposable Components
        entities.forEach{it.dispose()}
    }
}